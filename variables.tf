variable "region" {
  description = "AWS region"
}

variable "buildspec_tmpl" {}
variable "environment" {}

variable "resource_name_prefix" {
  description = "Resource name prefix"
}

variable "tags" {
type        = map(string)
default     = {}
description = "Tags map"
}

variable "policy_attachment_name" {
description = "Name of the policy attachment document"
default = "attachment"
}

#
# CodePipeline
#
variable "source_code_repo" {
type        = string
description = "Source code repository name"
}

variable "source_code_branch" {
type        = string
description = "Source code repository branch name"
default     = "develop"
}

variable "codestar_provider_type" {
  type = string
  description = "Provider type for CodeStar connection. Must be one of Bitbucket or GitHub"
  default = "Bitbucket"
}

#variable "vpc_id" {
#  type = string
#  description = "VPC for codebuild"
#  default = null
#}
#
#variable "vpc_security_group_ids" {
#  type = list(string)
#  description = "VPC security groups for CodeBuild"
#  default = null
#}
#
#variable "vpc_subnets" {
#  type = list(string)
#  description = "VPC subnets for codebuild"
#  default = null
#}

# -----------------------------------------------------------------------------
# Variables: CodeBuild
# -----------------------------------------------------------------------------
variable "build_image" {
  type        = string
  default     = "aws/codebuild/standard:2.0"
  description = "Docker image for build environment, e.g. 'aws/codebuild/standard:2.0' or 'aws/codebuild/eb-nodejs-6.10.0-amazonlinux-64:4.0.0'. For more info: http://docs.aws.amazon.com/codebuild/latest/userguide/build-env-ref.html"
}

variable "build_compute_type" {
  type        = string
  default     = "BUILD_GENERAL1_SMALL"
  description = "Instance type of the build instance"
}

variable "build_timeout" {
  default     = 5
  description = "How long in minutes, from 5 to 480 (8 hours), for AWS CodeBuild to wait until timing out any related build that does not get marked as completed"
}

variable "badge_enabled" {
  type        = bool
  default     = false
  description = "Generates a publicly-accessible URL for the projects build badge. Available as badge_url attribute when enabled"
}

variable "privileged_mode" {
  type        = bool
  default     = false
  description = "(Optional) If set to true, enables running the Docker daemon inside a Docker container on the CodeBuild instance. Used when building Docker images"
}

variable "build_variables" {
  type    = list(map(any))
  default = []
}

variable "vpc_config" {
  type    = list(any)
  default = []
}

variable "deploy_configuration" {
  description = "Used by Deploy stage. When deploying to S3 use BucketName and Extract keys. If deploy to ECS, use ClusterName, ServiceName, FileName keys."
  nullable    = true

  type = object({
    # Used by ECS deploys
    ClusterName = optional(string)
    ServiceName = optional(string)
    FileName    = optional(string)
    # Used by S3 deploys
    BucketName = optional(string)
    Extract    = optional(string)
  })
}