output "artifact_store_s3_bucket_arn" {
  value = aws_s3_bucket.artifact_store.arn
}

output "artifact_store_s3_bucket_id" {
  value = aws_s3_bucket.artifact_store.id
}

output "artifact_store_bucket_domain_name" {
  value = aws_s3_bucket.artifact_store.bucket_domain_name
}

output "artifact_store_bucket_regional_domain_name" {
  value = aws_s3_bucket.artifact_store.bucket_regional_domain_name
}

output "codebuild_role_name" {
  value = module.iam_role_codebuild.role_name
}

output "codebuild_role_arn" {
  value = module.iam_role_codebuild.role_arn
}
