resource "random_string" "postfix" {
  length   = 6
  numeric  = false
  upper    = false
  special  = false
  lower    = true
}

resource "aws_s3_bucket" "artifact_store" {
  bucket        = "${var.resource_name_prefix}-codepipeline-${random_string.postfix.result}"
  force_destroy = true
}

resource "aws_s3_bucket_ownership_controls" "artifact_store" {
  bucket = aws_s3_bucket.artifact_store.id

  rule {
    object_ownership = "BucketOwnerEnforced"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "artifact_store" {
  bucket = aws_s3_bucket.artifact_store.id

  rule {
    id = "rule-1"
    status = "Enabled"
    expiration {
      days = 5
    }
  }
}

module "iam_codepipeline" {
  source = "git::https://bitbucket.org/clouddistrictdevelopers/cd-aws-iam-role.git?ref=v0.0.3"

  region               = var.region
  resource_name_prefix = var.resource_name_prefix

  assume_role_policy = "${path.module}/policies/codepipeline-assume-role.json"
  template           = "${path.module}/policies/codepipeline-policies.json"
  role_name          = "codepipeline-role"
  policy_name        = "codepipeline-policy"

  role_vars = {
    codebuild_project_arn              = aws_codebuild_project.default.arn
    codestarconnections_connection_arn = aws_codestarconnections_connection.default.arn
    s3_bucket_arn                      = aws_s3_bucket.artifact_store.arn
  }
}

resource "aws_codestarconnections_connection" "default" {
  name          = "${var.resource_name_prefix}-repository-connection"
  provider_type = var.codestar_provider_type
}

resource "aws_codepipeline" "default" {
  name     = "${var.resource_name_prefix}-codepipeline"
  role_arn = module.iam_codepipeline.role_arn

  artifact_store {
    location = aws_s3_bucket.artifact_store.bucket
    type     = "S3"
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeStarSourceConnection"
      version          = "1"
      output_artifacts = ["source"]

      configuration = {
        ConnectionArn    = aws_codestarconnections_connection.default.arn
        FullRepositoryId = var.source_code_repo
        BranchName       = var.source_code_branch
      }
    }
  }

  stage {
    name = "Build"

    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      version          = "1"
      input_artifacts  = ["source"]
      output_artifacts = ["build_output"]

      configuration = {
        ProjectName = aws_codebuild_project.default.name
      }
    }
  }

  dynamic "stage" {
    for_each = length(keys(var.deploy_configuration)) > 0 ? [1] : []
    content {
      name = "Deploy"

      action {
        name            = "Deploy"
        category        = "Deploy"
        owner           = "AWS"
        provider        = "ECS"
        version         = "1"
        input_artifacts = ["build_output"]

        configuration = var.deploy_configuration
      }
    }
  }

  tags = var.tags
}

module "iam_role_codebuild" {
  source = "git::https://bitbucket.org/clouddistrictdevelopers/cd-aws-iam-role.git?ref=v0.0.3"

  region               = var.region
  resource_name_prefix = var.resource_name_prefix

  assume_role_policy = "${path.module}/policies/codebuild-assume-role.json"
  template           = "${path.module}/policies/codebuild-policy.json"
  role_name          = "codebuild-role"
  policy_name        = "codebuild-policy"

  role_vars = {
    s3_bucket_arn = aws_s3_bucket.artifact_store.arn
  }
}

resource "aws_codebuild_project" "default" {
  name          = "${var.resource_name_prefix}-codebuild"
  description   = "${var.resource_name_prefix}_codebuild_project"
  build_timeout = var.build_timeout
  badge_enabled = var.badge_enabled
  service_role  = module.iam_role_codebuild.role_arn

  artifacts {
    type           = "CODEPIPELINE"
    namespace_type = "BUILD_ID"
    packaging      = "ZIP"
  }

  concurrent_build_limit = "5"

  environment {
    compute_type    = var.build_compute_type
    image           = var.build_image
    type            = "LINUX_CONTAINER"
    privileged_mode = var.privileged_mode

    dynamic "environment_variable" {
      for_each = [for s in var.build_variables : {
        name      = lookup(s, "name", "")
        value     = lookup(s, "value", "")
      }]

      content {
        name      = environment_variable.value.name
        value     = environment_variable.value.value
      }
    }
  }

  source {
    type      = "CODEPIPELINE"
    buildspec = templatefile(var.buildspec_tmpl, {})
  }

  dynamic "vpc_config" {
    for_each = [for v in var.vpc_config : {
      vpc_id             = lookup(v, "vpc_id", null)
      security_group_ids = lookup(v, "security_group_ids", null)
      subnets            = lookup(v, "subnets", null)
    }]

    content {
      vpc_id             = vpc_config.value.vpc_id != "" ? vpc_config.value.vpc_id : ""
      subnets            = vpc_config.value.subnets != "" ? split(",", vpc_config.value.subnets) : []
      security_group_ids = vpc_config.value.security_group_ids != "" ? split(",", vpc_config.value.security_group_ids) : []
    }
  }
  tags = var.tags
}
